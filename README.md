# Sisop Modul 1 <br> Kelompok ITA 26



## Soal 1

### Cara Kerja

Kode ini adalah script bash yang memungkinkan pengguna untuk mengeksekusi beberapa fungsi terkait daftar universitas dunia dari QS World University Rankings. Berikut adalah langkah-langkah kerja dari kode tersebut:<br>


1. Membuat fungsi menu untuk menampilkan menu utama program dan meminta input dari pengguna untuk memilih salah satu opsi yang tersedia.

2. Fungsi menu1 ditambahkan untuk menampilkan 5 universitas dengan peringkat tertinggi di Jepang berdasarkan daftar QS World University Rankings.

3. Fungsi menu2 ditambahkan untuk mencari skor FSR (Faculty Student Score) terendah dari 5 universitas dengan peringkat tertinggi di Jepang.

4. Fungsi menu3 ditambahkan untuk mencari 10 universitas di Jepang dengan Employment Outcome Rank tertinggi.

5. Fungsi menu4 ditambahkan untuk mencari universitas dengan kata kunci tertentu dari nama universitas pada daftar QS World University Rankings.<br><br>
Program akan terus berjalan sampai pengguna memilih opsi "Keluar dari program" dari menu utama.
Jika pengguna memilih opsi yang tidak valid, program akan menampilkan pesan kesalahan.
Secara keseluruhan, kode ini memanfaatkan beberapa utilitas bash seperti awk, sort, dan head untuk memproses file csv yang berisi daftar universitas dan peringkat QS World University Rankings. Kode ini juga menggunakan variabel dan loop untuk memungkinkan pengguna memilih opsi yang berbeda dari menu utama program.

### Screenshot kode


### Screenshot Program


### Screenshot file

### Kendala yang Dialami
- ketika mengerjakan, CSV tidak dapat terbaca oleh program

## Soal 2

### Cara Kerja

Ini adalah skrip Bash sederhana yang melakukan download beberapa gambar dari Picsum setiap jam dan kemudian mengemasnya ke dalam folder dengan nomor folder yang berbeda dan mengompres folder setiap hari. Setelah itu, skrip ini menunggu 10 jam sebelum memulai kembali proses download. Skrip ini berjalan terus menerus (looping) sampai dihentikan secara manual.

Berikut adalah langkah-langkah kerja dari kode bash ini:

1. Mendefinisikan variabel folder_name dengan nilai awal kumpulan_1 dan folder_count dengan nilai awal 1.

2. Membuat folder baru dengan nama kumpulan_1 menggunakan perintah mkdir.

3. Memulai loop while.

4. Mendefinisikan variabel X yang berisi jam saat ini dengan format HH (24 jam).

5. Menghitung jumlah gambar yang akan didownload dengan mengisi nilai num_images dengan variabel X.

6. Melakukan loop sebanyak num_images kali untuk mendownload gambar dari https://picsum.photos/1200/800/?random menggunakan perintah curl dan menyimpan hasilnya dengan nama perjalanan_x.jpg di dalam folder dengan nama kumpulan_x.

7. Memeriksa apakah sudah waktunya melakukan zip folder. Jika jam saat ini sudah 00, maka melakukan zip pada folder dengan nama devil_x.zip yang berisi isi dari folder kumpulan_x. Setelah itu, membuat folder baru dengan nama kumpulan_y dan meningkatkan nilai dari variabel folder_count.

8. Menunggu selama 10 jam sebelum melanjutkan loop menggunakan perintah sleep 36000.

9. Jika belum waktunya melakukan zip, maka loop akan terus berjalan dan melakukan download gambar.

### Screenshot kode


### Screenshot Program


### Screenshot file


### Kendala yang Dialami
- Tidak dapat membuat file zip setelah dijalankan
- Tidak dapat membentuk versi awk dari program

## Soal 3

### Cara Kerja

- louis.sh

Script ini merupakan sebuah program Bash shell yang digunakan untuk mendaftarkan user baru pada sebuah sistem. Program ini terdiri dari beberapa fungsi yang digunakan untuk memeriksa persyaratan password baru dan untuk memastikan bahwa username yang dimasukkan belum terdaftar sebelumnya. Setelah memeriksa persyaratan password dan username, program akan menambahkan user baru ke dalam file users.txt dan mencatat proses pendaftaran dalam file log.txt. Jika username yang dimasukkan sudah terdaftar, maka program akan mengeluarkan pesan kesalahan dan keluar dari program. Jika password tidak memenuhi persyaratan, maka program akan meminta user untuk memasukkan password baru yang memenuhi persyaratan.<br><br>

Berikut adalah langkah-langkah cara kerja dari kode di atas:<br> <br>

1. Membuat direktori users jika belum ada dengan menggunakan perintah mkdir -p users.

2. Membuat file users.txt jika belum ada dengan menggunakan perintah touch users/users.txt.

3. Mendefinisikan fungsi check_password yang akan digunakan untuk memeriksa apakah password yang dimasukkan oleh pengguna memenuhi syarat.

4. Meminta pengguna untuk memasukkan nama pengguna (username) dengan menggunakan perintah read -p "Enter username: " username.

5. Mengecek apakah nama pengguna yang dimasukkan sudah terdaftar di dalam file users.txt dengan menggunakan perintah grep -q "^$username:" users/users.txt.

6. Jika nama pengguna sudah terdaftar, maka akan menampilkan pesan error dan keluar dari program.

7. Jika nama pengguna belum terdaftar, maka akan meminta pengguna untuk memasukkan password dengan menggunakan perintah read -s -p "Enter password: " password, di mana -s digunakan untuk menyembunyikan input password yang dimasukkan oleh pengguna.

8. Selama password yang dimasukkan tidak memenuhi syarat, program akan terus meminta pengguna untuk memasukkan password.

9. Jika password sudah memenuhi syarat, maka akan menambahkan nama pengguna dan password ke dalam file users.txt dengan menggunakan perintah echo "$username:$password" >> users/users.txt.

10. Menampilkan pesan berhasil dan menambahkan informasi log ke dalam file log.txt dengan menggunakan perintah echo "User $username registered successfully" dan echo "$(date +%Y/%m/%d\ %H:%M:%S) REGISTER: INFO User $username registered successfully" >> log.txt.

- retep.sh<br>

Script tersebut adalah script bash yang melakukan otentikasi username dan password untuk mengizinkan akses pada sistem. Pertama, script akan meminta input username dan password dari pengguna dan menyimpannya sebagai variabel. Kemudian, script akan memeriksa apakah username dan password yang dimasukkan oleh pengguna benar dengan memeriksa file /users/users.txt. Jika username dan password tidak benar, maka akan muncul pesan "Incorrect username or password" dan log akan ditulis dalam file log.txt dengan pesan "Failed login attempt on user $username" beserta timestamp. Jika otentikasi berhasil, maka akan muncul pesan "User $username logged in" dan log akan ditulis dalam file log.txt dengan pesan "User $username logged in" beserta timestamp.<br><br>

Langkah-langkah cara kerja dari kode ini adalah sebagai berikut:<br><br>

1. Meminta input dari pengguna untuk memasukkan username dan password dengan perintah read.

2. Memeriksa apakah username dan password yang dimasukkan oleh pengguna benar atau tidak dengan menggunakan perintah grep.

3. Jika username dan password benar, cetak pesan "User <username> logged in" ke konsol dan catat tindakan masuk log dengan menggunakan perintah echo dan menambahkan baris pada file log.txt.

4. Jika username dan password salah, cetak pesan "Incorrect username or password" ke konsol dan catat tindakan masuk yang gagal pada file log.txt.

5. Keluar dari program dengan menggunakan perintah exit.

### Screenshot kode


### Screenshot Program


### Screenshot file


### Kendala yang Dialami
_Tidak ada kendala dalam mengerjakan soal ini_

## Soal 4

### Cara Kerja

- log_encrypt.sh

Langkah-langkah cara kerja dari kode tersebut adalah sebagai berikut:

1. Mendapatkan waktu saat ini dengan format jam dalam variabel current_time.

2. Menghitung jumlah pergeseran shift pada sandi dengan memoduluskan current_time dengan 26, lalu menyimpannya dalam variabel shift.

3. Membuat nama file backup dengan format syslog_Jam:Menit_Tanggal:Bulan:Tahun.txt dalam variabel backup_file.

4. Mengenkripsi isi file syslog dengan menerapkan sandi Caesar cipher menggunakan pergeseran shift yang sudah dihitung sebelumnya. Hasil enkripsi disimpan dalam file backup yang sudah dibuat tadi.

5. Menampilkan pesan "Syslog backup file created and encrypted at $backup_file".

<br>


- log_decrypt.sh

Langkah-langkah cara kerja dari kode tersebut adalah sebagai berikut:<br><br>
1. Pertama-tama, program akan meminta input dari user berupa nama file backup terenkripsi yang akan didekripsi.

2. Program akan mengambil informasi waktu pada nama file untuk menentukan nilai shift yang digunakan untuk enkripsi cipher. Informasi waktu ini diambil dengan menggunakan fungsi sed untuk mengambil bagian dari nama file tertentu. Timestamp kemudian disalin ke variabel $timestamp.

3. Nilai shift dihitung berdasarkan waktu pada timestamp menggunakan perintah date. Nilai shift ini diambil dari jam pada waktu timestamp dan disalin ke variabel $shift.

4. Nama file hasil dekripsi disimpan dalam variabel $decrypted_file. Nama file ini dihasilkan dengan mengganti ekstensi dari nama file terenkripsi menjadi "_decrypted.txt".

5. Setelah nilai shift dan nama file terdekripsi dihitung, program akan melakukan dekripsi terhadap isi file terenkripsi menggunakan perintah tr. Isi file terenkripsi diambil dari file input yang diambil dari input user, dan output dari perintah tr disimpan ke dalam file terdekripsi yang telah dibuat sebelumnya.

6. Program mencetak pesan yang memberi tahu user bahwa proses dekripsi telah selesai dan file terdekripsi disimpan pada alamat yang telah ditentukan.

### Screenshot kode


### Screenshot Program


### Screenshot file


### Kendala yang Dialami
- Tidak dapat mendekripsi hasil enkripsi
- Tidak dapat menjalankan awk pada program
